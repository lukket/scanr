# Scanr

Scan barcodes for your inventory and process them in Excel.

## Features

- Scan barcodes from any angle (1D and 2D\*)
- Audio beep and visual feedback
- History of barcodes stored in browser
- Export to Excel compatible CSV file
- Screen wake lock

[*] 2D barcodes are disabled in code for performance reasons. Feel free to enable it.

## Requirements

Your device and browser must support the experimental [Barcode Detection API](https://developer.mozilla.org/docs/Web/API/Barcode_Detection_API).

### iOS 17

If you use an iPhone or iPad go to Settings > Safari > Advanced > Feature Flags
and enable Shape Detection API.

## Devices

- iPhone 11 and above with iOS version 17 and above
  - Best results: iPhone Pro with macro photography.

## Export

You can export scanned barcodes to an Excel compatible format. For every scanned
barcode, a line with the following format will be added.

`ID;SCANNED_AT;CODE`

Where `ID` is the Unix timestamp in milliseconds, `SCANNED_AT` is the timestamp
as `YYYY-MM-DD hh:mm:ss` and `CODE` is the raw barcode value.

## Try it out

[![](./docs/images/frame.png)](https://scanr.lukket.me)

## Screenshots

![](./docs/images/screenshot_01.jpeg)
![](./docs/images/screenshot_02.jpeg)
![](./docs/images/screenshot_03.jpeg)
