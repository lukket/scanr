import { AsyncPipe, DatePipe } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { FormsModule } from '@angular/forms';
import { BarcodeScan, HistoryService } from '../history.service';

export function toExcelCsv(scans?: BarcodeScan[]) {
  if (!scans) {
    return '';
  }

  const excelCsv = scans.map((scan) => {
    const isoDateString = scan.scannedAt.toISOString().split('T');
    const date = isoDateString[0];
    const time = isoDateString[1].split('.')[0];
    return `${scan.id};${date} ${time};${scan.code}`;
  });

  return excelCsv.join('\n');
}

@Component({
  selector: 'scanr-history',
  standalone: true,
  templateUrl: './history.component.html',
  styleUrl: './history.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [AsyncPipe, DatePipe, FormsModule],
})
export class HistoryComponent {
  private readonly historyService = inject(HistoryService);

  protected readonly scans = toSignal(this.historyService.scanHistory$);
  protected readonly canShare = typeof navigator.share === 'function';
  protected filename?: string;

  protected onRemove(scan: BarcodeScan): void {
    if (
      window.confirm(
        `Do you really want to remove barcode ${scan.code} from the history?`,
      )
    ) {
      this.historyService.remove(scan.id);
    }
  }

  protected onClear() {
    if (window.confirm('Do you really want to clear the barcode history?')) {
      this.historyService.clear();
    }
  }

  protected async onShare(scans?: BarcodeScan[]) {
    const blob = new Blob([toExcelCsv(scans)], {
      type: 'text/csv',
    });
    const file = new File([blob], `${this.filename ?? 'barcodes'}.csv`);

    const data: ShareData = { files: [file] };

    if (!navigator.canShare(data)) {
      alert('Sharing files is not supported by you device.');
      return;
    }

    try {
      await navigator.share(data);
    } catch (error) {
      console.error('An unexpected error occurred during sharing.', error);
    }
  }
}
