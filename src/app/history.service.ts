import { Injectable } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { BehaviorSubject, skip, tap } from 'rxjs';

export type BarcodeScan = {
  id: number;
  scannedAt: Date;
  code: string;
};

type JsonEncodedBarcodeScan = Omit<BarcodeScan, 'scannedAt'> & {
  scannedAt: string;
};

const LOCAL_STORAGE_KEY = 'Scanr.ScanHistory';

@Injectable({ providedIn: 'root' })
export class HistoryService {
  private readonly scanHistory$$ = new BehaviorSubject<BarcodeScan[]>([]);

  public readonly scanHistory$ = this.scanHistory$$.asObservable();

  constructor() {
    const historyFromStorageString = localStorage.getItem(LOCAL_STORAGE_KEY);
    if (historyFromStorageString !== null) {
      const encodedHistoryFromStorage: JsonEncodedBarcodeScan[] = JSON.parse(
        historyFromStorageString,
      );

      const historyFromStorage: BarcodeScan[] = encodedHistoryFromStorage.map(
        (scan) => ({
          ...scan,
          scannedAt: new Date(scan.scannedAt),
        }),
      );

      this.scanHistory$$.next(historyFromStorage);
    }

    this.scanHistory$
      .pipe(
        // Don't store the current state from storage.
        skip(1),
        tap((scanHistory) => {
          localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(scanHistory));
        }),
        takeUntilDestroyed(),
      )
      .subscribe();
  }

  public add(code: string, scannedAt: Date = new Date()): BarcodeScan {
    const identifiableScan = {
      id: Date.now(),
      code,
      scannedAt,
    };
    this.scanHistory$$.next([...this.scanHistory$$.value, identifiableScan]);
    return identifiableScan;
  }

  public remove(scanId: number): void {
    const indexToRemove = this.scanHistory$$.value.findIndex(
      (scan) => scan.id === scanId,
    );

    if (indexToRemove === -1) {
      return;
    }

    const scanHistory = [...this.scanHistory$$.value];
    scanHistory.splice(indexToRemove, 1);
    this.scanHistory$$.next(scanHistory);
  }

  public clear(): void {
    this.scanHistory$$.next([]);
  }
}
