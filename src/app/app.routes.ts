import { inject } from '@angular/core';
import { CanActivateFn, Route, Router } from '@angular/router';
import { AppComponent } from './app.component';
import { DeviceNotSupportedComponent } from './device-not-supported/device-not-supported.component';
import { HistoryComponent } from './history/history.component';
import { ScannerComponent } from './scanner/scanner.component';

export const deviceSupportGuard: CanActivateFn = () => {
  const isBarcodeDetectorSupported = 'BarcodeDetector' in globalThis;
  return (
    isBarcodeDetectorSupported ||
    inject(Router).createUrlTree(['device-not-supported'])
  );
};

export const appRoutes: Route[] = [
  {
    path: '',
    component: AppComponent,
    canActivate: [deviceSupportGuard],
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'scanner' },
      {
        path: 'scanner',
        loadComponent: () => ScannerComponent,
      },
      {
        path: 'history',
        loadComponent: () => HistoryComponent,
      },
    ],
  },
  {
    path: 'device-not-supported',
    loadComponent: () => DeviceNotSupportedComponent,
  },
];
