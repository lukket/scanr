import { Component } from '@angular/core';
import { RouterModule } from '@angular/router';

@Component({
  standalone: true,
  selector: 'scanr-root',
  template: `<router-outlet />`,
  imports: [RouterModule],
})
export class RootComponent {}
