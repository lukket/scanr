import { Component } from '@angular/core';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'scanr-device-not-supported',
  standalone: true,
  imports: [RouterLink],
  templateUrl: './device-not-supported.component.html',
  styleUrl: './device-not-supported.component.scss',
})
export class DeviceNotSupportedComponent {}
