import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DeviceNotSupportedComponent } from './device-not-supported.component';

describe('DeviceNotSupportedComponent', () => {
  let component: DeviceNotSupportedComponent;
  let fixture: ComponentFixture<DeviceNotSupportedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DeviceNotSupportedComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(DeviceNotSupportedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
