import { Component, inject } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import { map } from 'rxjs';
import { HistoryService } from './history.service';

@Component({
  standalone: true,
  selector: 'scanr-app',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
  imports: [RouterLink, RouterLinkActive, RouterOutlet],
})
export class AppComponent {
  protected numberOfBarcodes = toSignal(
    inject(HistoryService).scanHistory$.pipe(map((scans) => scans.length)),
  );
}
