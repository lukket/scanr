import { animate, style, transition, trigger } from '@angular/animations';
import { NgIf } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  ViewChild,
  inject,
} from '@angular/core';
import { HistoryService } from '../history.service';
import { beep } from '../utils/beep';

@Component({
  selector: 'scanr-scanner',
  standalone: true,
  templateUrl: './scanner.component.html',
  styleUrl: './scanner.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('detectedBarcodeAnimation', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('200ms', style({ opacity: '*' })),
      ]),
      transition(':leave', [
        style({ opacity: '*' }),
        animate('200ms', style({ opacity: '0' })),
      ]),
    ]),
  ],
  imports: [NgIf],
})
export class ScannerComponent implements OnDestroy {
  private readonly historyService = inject(HistoryService);
  private readonly cdr = inject(ChangeDetectorRef);
  private readonly barcodeDetector!: BarcodeDetector;
  private wakeLock?: WakeLockSentinel;

  @ViewChild('video', { static: true })
  protected videoRef!: ElementRef<HTMLVideoElement>;

  protected readonly isBarcodeDetectorSupported: boolean;
  protected isScanning = false;
  protected detectedBarcode?: string;

  constructor() {
    this.isBarcodeDetectorSupported = 'BarcodeDetector' in globalThis;

    if (this.isBarcodeDetectorSupported) {
      this.barcodeDetector = new BarcodeDetector({
        formats: [
          'code_128',
          'code_39',
          'code_93',
          'codabar',
          'ean_13',
          'ean_8',
          'itf',
          'upc_e',
        ],
      });
      // this.barcodeDetector = new BarcodeDetector();
    }

    this.requestWakeLock();
    this.startCamera();
  }

  private async startCamera(): Promise<void> {
    const stream = await navigator.mediaDevices.getUserMedia({
      video: {
        facingMode: { ideal: 'environment' },
      },
    });

    this.videoRef.nativeElement.srcObject = stream;
    this.videoRef.nativeElement.play();
  }

  protected async scan(): Promise<void> {
    const scanner = this.barcodeDetector;

    this.isScanning = true;
    const intervalId = setInterval(async () => {
      const codes = await scanner.detect(this.videoRef.nativeElement);

      if (codes.length <= 0) {
        return;
      }

      this.historyService.add(codes[0].rawValue);
      this.detectedBarcode = codes[0].rawValue;
      this.cdr.detectChanges();

      setTimeout(() => {
        this.detectedBarcode = undefined;
        this.cdr.detectChanges();
      }, 500);

      clearInterval(intervalId);
      beep();
      this.isScanning = false;
    }, 250);
  }

  private async requestWakeLock(): Promise<void> {
    try {
      this.wakeLock = await navigator.wakeLock.request('screen');
    } catch (error: unknown) {
      console.warn('Cannot activate wake lock.', error);
    }
  }

  protected onPause(): void {
    // On iOS the video stops from time to time,
    // but we want to not pause the video at any time.
    this.videoRef.nativeElement.play();
  }

  public ngOnDestroy(): void {
    this.wakeLock?.release();
  }
}
